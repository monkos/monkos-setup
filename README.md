# Monk OS Setup

[monkos-setup](https://gitlab.com/monkos/monkos-setup) is a set of scripts and
configuration files that could be used to build [Monk OS](https://gitlab.com/monkos)
from scratch. These scripts provide options to get the [yocto](https://www.yoctoproject.org)
layers from different repositories and create required folder structures.

**How to use the script**

    source src/monkos-setup/script/setup-build-env -h
    Usage:
     script/setup-build-env [options]
            -f           only fetch the source
            -d <device>  specifies which device class to be used
                         if nothing is specified, device will be defaulted to raspberrypi
            -l           list supported devices
            -i           initialize the build enviornment
            -b <branch>  specifies which branch/tag to be used
                         if nothing is specified, branch will be defaulted to master
            -a           list available branches/tags
            -m           select machine manually
            -s           do a forced sync of source
            -h           help



**You could also contribute:**
    The current state of the script is very crude and need to enhanced.

**Maintainer:**
* Johan Saji <johansaji.dev@gmail.com>
