#!/bin/bash

# MIT License

# Copyright (c) 2018 Johan Saji<johansaji.dev@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# this script is expected to run in a bash env
if [ -z "$BASH" ]; then
    echo -e "ERROR: script needs to be run in a bash shell."
    return 1
fi

# this script should not be run as a root user
if [ "$(whoami)" = "root" ]; then
    echo -e "ERROR: do not run this script as a root user"
    return 1
fi

# check if you are passing correct number of arguments
if [ "$#" -ne 3 ]; then
    echo -e  "ERROR: invalid number of arguments"
    exit 1
fi

_manifest_url=$1
_manifest_name=$2
_manifest_tag=$3

fetch_from_remote() {
    repo init -u ${_manifest_url} -m ${_manifest_name} -b ${_manifest_tag}
    echo -e "INFO: repo initialized with url ${_manifest_url} manifest ${_manifest_name} from tag ${_manifest_tag}"
    #repo sync -c --no-clone-bundle -j 4
    repo sync -c --no-clone-bundle --no-tags --optimized-fetch -j 4
}

fetch_from_remote

# clean up
unset _manifest_url _manifest_name  _manifest_tag
